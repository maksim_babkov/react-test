import { useState } from 'react';
import './modalDialog.scss'

interface IProps {
    isOpen: boolean
    closeModal: () => void
}

const ModalDialog = (props: IProps) => {

    const [fullWidth, setFullWidth] = useState<boolean>(false);

    const [minimizeModal, setMinimizeModal] = useState(false);

    const startFullWidth = (boolean: boolean): any => {
        if (boolean === false) return setFullWidth(true)
        if (boolean === true) return setFullWidth(false)
    }

    return (
        <>
            {minimizeModal ?
                <div className='minimize-modal'
                    onClick={() => setMinimizeModal(false)}
                >
                    <span className='minimize-text'>mvc</span>
                </div>
                :
                <div className={props.isOpen ? "ModalDialog" : "ModalDialog-close"}
                >
                    <div className={fullWidth ? "full-width-content" : "content"}>
                        <div className="heder">
                            <div className="heder-flex-element">
                                <span className='stile-text-mvs'>MVC</span>
                            </div>

                            <div className="heder-flex-element">
                                <div className="heder-close-flex">
                                    <div className="heder-close-flex-element">
                                        <div className='style-yellow'
                                            onClick={() => setMinimizeModal(true)}
                                        ></div>
                                    </div>
                                    <div className="heder-close-flex-element">
                                        <div className='style-green'
                                            onClick={() => setFullWidth(() => startFullWidth(fullWidth))}
                                        ></div>
                                    </div>
                                    <div className="heder-close-flex-element">
                                        <div className='style-red'
                                            onClick={() => {
                                                props.closeModal()
                                                setFullWidth(false)
                                            }
                                            }
                                        ></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="main">
                            <h1>Content</h1>
                        </div>
                        <div className="footer">
                            <div>Footer</div>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}

export default ModalDialog;