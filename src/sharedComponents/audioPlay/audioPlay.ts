const shortclick = require(
    './audio/knopka-schelchok-odinochnyii-korotkii-myagkii-blizkii1.mp3')
const doubleСlick = require(
        './audio/knopka-schelchok-zvonkii-blizkii-dvoinoi1.mp3')

const engineSound = require(
            './audio/need_for_speed_most_wanted_07 - Hush-Fired up.mp3')
            const engineEffect = require('./audio/Sound Efects - Двиг + отсечка (www.hotplayer.ru).mp3')


export default class AudioStart {

    private static audio: any  = new Audio(engineSound) 
    private static audioShortclick: any = new Audio(shortclick)
    private static audioDoubleСlick: any = new Audio(doubleСlick) 
    private static audioEngineEffect: any = new Audio(engineEffect) 

    public static shortclick = () => this.audioShortclick.play()
    public static doubleСlick = () => this.audioDoubleСlick.play()


    public static engineSoundEffect = (state: boolean) => {
        if(state === true) this.audioEngineEffect.play()
        if(state === false) 
        this.audioEngineEffect.pause() 
        this.audioEngineEffect.currentTime = 0;
     }

    public static engineSound = (state: boolean) => {
       if(state === true) this.audio.play()
       if(state === false) 
       this.audio.pause() 
       this.audio.currentTime = 0;
    }
}

  




