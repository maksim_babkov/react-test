import './alertCostoms.scss'

interface IAlertProps {
    textBody: string
    errorText?: string
    positionStyle: number
    shouAlert?: boolean
}

const AlertCostoms = (props: IAlertProps) => {

    return (
        <div
            style={{
                position: 'absolute',
                top: '30px',
                left: '130px'
            }}
            className={props.shouAlert ? 'display' : 'displayNone'}
            >
            <div className="AlertCostoms">
                <div>{props.textBody}</div>
            </div>
        </div>
    )
}

export default AlertCostoms