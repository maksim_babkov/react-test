import AlertCostoms from "../../sharedComponents/alertCastoms/alertCastoms";
import SidebarMenu from "../AppStart/components/SidebarMenu";
import { useState } from 'react';

const AlignRight = () => {

    const [alert, setAlert] = useState<boolean>(false)

    const [textAlertBody, setTextAlertBody] = useState<string>('')

    let alertActyve = (bul: boolean) => {
        setAlert(bul)
    }

    let changeAlert = (state: boolean, textBody: string) => {
        alertActyve(state)
        setTextAlertBody(textBody)
    }

    return (
        <div className="Loyout">
            <div className="AlignRight">
                <SidebarMenu />

                <div style={{ width: '300px', margin: '0 auto' }}>
                    <h1
                        onMouseOver={() => changeAlert(true, 'AlignRight')}
                        onMouseOut={() => changeAlert(false, '')}
                        style={{ textAlign: "center", cursor: "pointer", position: "relative" }}
                    >AlignRight</h1>
                </div>

                <div style={{ width: '300px', margin: '0 auto' }}>
                    <h1
                        onMouseOver={() => changeAlert(true, 'Al777')}
                        onMouseOut={() => changeAlert(false, '')}
                        style={{ textAlign: "center", cursor: "pointer", position: "relative" }}
                    >Al777</h1>
                </div>


                <AlertCostoms
                    textBody={textAlertBody}
                    positionStyle={0}
                    shouAlert={alert}
                />
            </div>
        </div>
    )
}

export default AlignRight;