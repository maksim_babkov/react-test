import { useState } from 'react';
import SidebarMenu from '../AppStart/components/SidebarMenu';
import ModalDialog from '../../sharedComponents/modalDialog/modalDialog';
import './home.scss'
import AudioStart from '../../sharedComponents/audioPlay/audioPlay';

const Home = () => {

    const [isOpen, setIsOpen] = useState(false);

    const openModal = () => setIsOpen(true)
    const closeModal = () => setIsOpen(false)

    return (
        <div className="Loyout">
            <div className="Home">
                <SidebarMenu />
                <h1 style={{ textAlign: "center" }}>Home</h1>
                <button
                    className='button-modal'
                    onClick={() => {
                        AudioStart.doubleСlick()
                        openModal()
                    }}>
                    OpenModal
                </button>
                <ModalDialog
                    isOpen={isOpen}
                    closeModal={closeModal}
                />
            </div>
        </div>
    )
}

export default Home;




