import { useState } from "react";
import AudioStart from '../../../../sharedComponents/audioPlay/audioPlay'
import './car.scss'
const wheel = require('./image/wheel-png-hd-front-hd-wheel-hd-png-980.png')

const Car = () => {

    const [rotate, setRotate] = useState<string>('')
    const [wheelSpeed, setWheelSpeed] = useState<string>('wheel')
    const [stateSpeed, setStateSpeed] = useState<string>('speed-1')
    const [boxColor, setBoxColor] = useState<string>('white')
    const [count, setCount] = useState<number>(0)

    const onChangeCount = (count: number) => {
        switch (count) {
            case 0:
                setBoxColor('red')
              break;
            case 1:
                setBoxColor('yellow')
              break;
            case 2:
                setBoxColor('blue')
              break;
            default:
                setBoxColor('white')
          }
    }

    return (
        <div className="car">

            <button className="button-btn-style"
                onClick={() => AudioStart.engineSound(true)}
            >
                play
            </button>

            <button className="button-btn-style"
                onClick={() => AudioStart.engineSound(false)}
            >
                pause
            </button>

            <button className="button-btn-style"
                onMouseDown={() => {
                    setRotate('rotate')
                    setWheelSpeed('wheel-speed')
                    AudioStart.engineSoundEffect(true)
                    setStateSpeed('speed-2')
                }}
                onMouseUp={() => {
                    setRotate('')
                    setWheelSpeed('wheel')
                    AudioStart.engineSoundEffect(false)
                    setStateSpeed('speed-1')
                }}
            >
                speed
            </button>

            <button className="button-btn-style"
                onClick={() => {
                    if(count <= 2) { setCount(count + 1)}
                    else{setCount(0)}
                    onChangeCount(count)
                }
            }
            >
                color
            </button>

            <div className="car-container">

                <div className="marquee">
                    <div className={stateSpeed}>
                        <div className="sped-block"></div>
                    </div>
                </div>


                <div className='leftWheel'>
                    <img className={wheelSpeed} src={wheel} alt="" width={150} />
                </div>

                <div className='rightWheel'>
                    <img className={wheelSpeed} src={wheel} alt="" width={150} />
                </div>

                <div className={`box-color-center rotate ${boxColor}`}></div>

                <img className={rotate} src="https://www.pngpix.com/wp-content/uploads/2016/06/PNGPIX-COM-White-Lamborghini-Aventador-LP-Car-PNG-Image.png" alt="" width="100%" />
            </div>
        </div>
    )
}

export default Car;