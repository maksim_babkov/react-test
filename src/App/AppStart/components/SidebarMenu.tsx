import { faHome, faHamsa, faAlignRight, faLandmark, faAddressBook, faKeyboard, faAdjust, faVanShuttle, faFileZipper, faLocation, faMap, faHSquare, faAppleAlt, faYen, faYinYang } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { Link } from "react-router-dom";
import AudioStart from "../../../sharedComponents/audioPlay/audioPlay";



export const SidebarMenu = () => {

    const [mousText, mouseOverText] = useState('margin-left');
    const [mousBlock, mouseOverBlock] = useState('SidebarMenu');

    return (

        <div className={mousBlock}
            onMouseOver={() => {
                mouseOverText('margin-left-action button_slide slide_down');
                mouseOverBlock('SidebarMenu-action');
            }}
            onMouseOut={() => {
                mouseOverText('margin-left');
                mouseOverBlock('SidebarMenu');
            }}
            onClick={() => {
                mouseOverText('margin-left');
                mouseOverBlock('SidebarMenu');
            }}
        >
            <div className="my-flex-cont">

                <h3 style={{ textAlign: "center", color: "white" }}>
                    <Link 
                    onClick={AudioStart.doubleСlick}
                    onMouseOver={AudioStart.shortclick} 
                    to="/">MVC</Link>
                </h3>

                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faHome} />
                        <div className={mousText}>
                            <div><Link 
                            onClick={AudioStart.doubleСlick}
                            onMouseOver={AudioStart.shortclick} to="/home">Home</Link></div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faHamsa} />
                        <div className={mousText}>
                            <div><Link 
                            onClick={AudioStart.doubleСlick}
                            onMouseOver={AudioStart.shortclick} to="/hamsa">Hamsa</Link></div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faAlignRight} />
                        <div className={mousText}>
                            <div><Link 
                            onClick={AudioStart.doubleСlick}
                            onMouseOver={AudioStart.shortclick}
                            to="/alignright">AlignRight</Link></div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faLandmark} />
                        <div className={mousText}
                        >
                            <div>Landmark</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faAddressBook} />
                        <div className={mousText}
                        >
                            <div>AddressBook</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faKeyboard} />
                        <div className={mousText}
                        >
                            <div>Keyboard</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faAdjust} />
                        <div className={mousText}
                        >
                            <div>Adjust</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faVanShuttle} />
                        <div className={mousText}
                        >
                            <div>VanShuttle</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faFileZipper} />
                        <div className={mousText}
                        >
                            <div>FileZipper</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faLocation} />
                        <div className={mousText}
                        >
                            <div>Location</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faMap} />
                        <div className={mousText}
                        >
                            <div>Map</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faHSquare} />
                        <div className={mousText}
                        >
                            <div>HSquare</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faYinYang} />
                        <div className={mousText}
                        >
                            <div>YinYang</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faYen} />
                        <div className={mousText}>
                            <div>Yen</div>
                        </div>
                    </div>
                </div>
                <div className="my-flex-box">
                    <div className="collor-text">
                        <FontAwesomeIcon icon={faAppleAlt} />
                        <div className={mousText}
                        >
                            <div>AppleAlt</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SidebarMenu;