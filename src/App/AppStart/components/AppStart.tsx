import App from "./App";
import SidebarMenu from "./SidebarMenu";

const AppStart = () => {
    return (
        <div>
            <h1 style={{ textAlign: "center"}}>AppStart</h1>
            <SidebarMenu />
            <App />
        </div>
    )
}

export default AppStart;