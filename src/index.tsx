import { render } from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./App/home/home";
import AppStart from './App/AppStart/components/AppStart';
import Hamsa from './App/hamsa/hamsa';
import './index.scss'
import AlignRight from "./App/alignright/alignRight";

render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<AppStart />} />
      <Route path="/home" element={<Home />} />
      <Route path="/hamsa" element={<Hamsa />} />
      <Route path="/alignright" element={<AlignRight />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById("root")
);

